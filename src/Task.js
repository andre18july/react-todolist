import React, { Component } from 'react';

class Task extends Component{


    handleEdit = (id) => {
        this.props.editTask(this.props.id);
    }

    handleDel = (id) =>{
        this.props.delTask(this.props.id);
    }

    render(){


        return(
            <li style={{margin: '20px 0', backgroundColor: '#f0f0f0', padding: '10px'}}>
                <div style={{display: 'inline-block', width: '85%', textAlign: 'left'}}>{this.props.desc}</div>
                <span 
                    style={{cursor: 'pointer' , backgroundColor: 'lightBlue', padding: '5px', fontSize: '12px', marginRight: '10px', textAlign: 'rigth'}}
                    onClick={() => this.handleEdit(this.props.id)}
                > EDIT </span>
                <span 
                    style={{cursor: 'pointer' , backgroundColor: 'pink', padding: '5px', fontSize: '12px', textAlign: 'right'}}
                    onClick={() => this.handleDel(this.props.id)}    
                > DEL </span>
            </li>
        )
    }

}

export default Task;
import React , { Component } from 'react';

class EditTask extends Component {

    constructor(props){
        super(props);

        this.state = {
            modTask: {id: '', desc: '', editing: ''}
        }

    }


    handleSave = (event) => {
        const savedTask = { id: this.props.id , desc: this.state.modTask.desc, editing: false }

        this.props.saveTask(savedTask, this.props.index);
    }

    handleModTask = (event) => {
        const newTask = { id: this.props.id , desc: event.target.value, editing: this.props.editing }

        this.setState({
            modTask: newTask
        })
    }

    render(){

        return(
            <li style={{margin: '20px 0', backgroundColor: '#ffffcc', padding: '10px'}}>
                <input style={{display: 'inline-block', width: '90%', textAlign: 'left'}}
                    name='modTask' 
                    placeholder={this.props.desc}
                    onChange={this.handleModTask}
                    value={this.state.modTask.desc}
                />

                <span 
                    style={{cursor: 'pointer' , backgroundColor: 'lighgreen', padding: '5px', fontSize: '12px', textAlign: 'right'}}
                    onClick={this.handleSave}    
                > SAVE </span>
            </li>
        )
    }
}

export default EditTask;
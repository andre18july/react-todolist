import React, { Component } from 'react';
import Task from './Task.js';
import NewTaskForm from './NewTaskForm';
import uuid from 'uuid';
import EditTask from './EditTask.js';


class TodoList extends Component {

    constructor(props){
        super(props);

        this.state = {
            tasks: [{id: uuid(), desc: 'take a bath', editing: false}, {id: uuid(), desc: 'have lunch with grandma', editing: false}]
        }
    }

    addTask = (task) => {
        let newtask = {id: uuid(), desc: task, editing: false}
        this.setState(prev => ({
            tasks: [...prev.tasks, newtask]
        }))
    }

    delTask = (id) => {
        const newTasks = this.state.tasks.filter(task => task.id !== id);
        this.setState({
            tasks: newTasks
        })
    }

    editTask = (id) => {

        let newTasks = this.state.tasks.filter(task => task.id !== id);
        const editTask = this.state.tasks.filter((task) => task.id === id)[0];
        const indexTask = this.state.tasks.indexOf(editTask);
        editTask.editing = true;

        newTasks.splice(indexTask, 0, editTask);
        
        this.setState({
            tasks: newTasks
        })
    }

    saveTask = (task, index) => {
        let newTasks = this.state.tasks.filter(t => t.id !== task.id);
        newTasks.splice(index, 0, task);

        this.setState({
            tasks: newTasks
        })
    }

    render(){

        const tasks = this.state.tasks.map(task => {
            if(!task.editing){
                return (
                    <Task 
                        key={uuid()} 
                        id={task.id} 
                        desc={task.desc} 
                        delTask={this.delTask}
                        editTask={this.editTask}
                    />
                )
            }else{
                return(
                    <EditTask 
                        key={uuid()} 
                        id={task.id}
                        index={this.state.tasks.indexOf(task)}
                        desc={task.desc} 
                        editing={task.editing}
                        saveTask={this.saveTask}
                    />
                )
            }
        })
        
        return(
            <div style={{margin: '50px auto', width: '800px', backgroundColor: '#f8f8f8', padding: '20px 50px'}}>
                <h1 style={{marginBottom: '50px'}}>TodoList 2019</h1>
                <ul style={{listStyleType: 'none', paddingLeft: '0px'}}>{tasks}</ul>
                <NewTaskForm addTask={this.addTask}/>

            </div>
        )
    }
}

export default TodoList;
import React, { Component } from 'react';

class NewTaskForm extends Component {

    constructor(props){
        super(props);

        this.state = {
            newtodo: ''
        }
    }

    handleNewTodo = (event) => {
        this.setState({
            newtodo: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.addTask(this.state.newtodo);
        this.setState({
            newtodo: ''
        })
    }

    render(){

        return(
            <div style={{textAlign: 'left', marginTop: '50px'}}>
                <h3>New Todo</h3>
                <form onSubmit={this.handleSubmit}>
                    <input style={{width: '400px', display: 'inline-block', marginRight: '10px'}} 
                        name='newtodo' 
                        placeholder='New Todo' 
                        value={this.state.newtodo}
                        onChange={this.handleNewTodo}/>
                    <button>Add Todo</button>
                </form>
            </div>
        )
    }
}

export default NewTaskForm;